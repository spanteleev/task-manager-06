package ru.tsc.panteleev.tm;

import ru.tsc.panteleev.tm.constant.ArgumentConst;
import ru.tsc.panteleev.tm.constant.TerminalConst;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Application {

    public static void main(String[] args) throws IOException {
        if (run(args))
            System.exit(0);
        showWelcome();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.println("Enter command:");
            String command = bufferedReader.readLine();
            runWithCommand(command);
        }
    }

    public static void runWithCommand(String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            default:
                showErrorCommand(command);
                break;
        }
    }

    public static void runWithArgument(String command) {
        switch (command) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            default:
                showErrorArgument(command);
                break;
        }
    }

    public static boolean run(String[] args) {
        if (args == null || args.length == 0) return false;
        final String command = args[0];
        if (command == null || command.isEmpty()) return false;
        runWithArgument(command);
        return true;
    }

    public static void exit() {
        System.exit(0);
    }

    public static void showWelcome() {
        System.out.println("Welcome to Task Manager");
    }

    public static void showAbout() {
        System.out.println("Name: Sergey Panteleev");
        System.out.println("E-mail: spanteleev@t1-consulting.ru");
    }

    public static void showVersion() {
        System.out.println("1.6.0");
    }

    public static void showHelp() {
        System.out.printf("Command '%s' or argument '%s' - Show developer info. \n", TerminalConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("Command '%s' or argument '%s' - Show application version. \n", TerminalConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("Command '%s' or argument '%s' - Show terminal commands. \n", TerminalConst.HELP, ArgumentConst.HELP);
        System.out.printf("Command '%s' - Close applicaion. \n", TerminalConst.EXIT);
    }

    public static void showErrorCommand(String command) {
        System.err.printf("Error! This command `%s` not supported... \n", command);
        showHelp();
    }

    public static void showErrorArgument(String command) {
        System.err.printf("Error! This argument `%s` not supported... \n", command);
        showHelp();
    }

}
